Issue Tracker
==============

Introduction
------------
Issue Tracker is a small .NET application that represents a minimal implementation of a helpdesk system, wherein users with different roles can report, assign, and resolve issues.

Developed from scratch as part of related coursework at Ball State University, this project interfaces with an embedded database via SQL to store user and issue data.

Technologies
------------
C#
SQL
CSS
Microsoft Access (embedded database)
Visual Studio
BitBucket

Requirements
------------
+ Visual Studio 2017+
+ Web browser

Installation
------------
To avoid deployment to an IIS server, this application can be run direclty from within Visual Studio using IISExpress.

Use
---
+ **Create an issue**
    1. With the web application launched, navigate to /login.aspx.
    2. Login with the *tester* role using the following credentials:
    
       **Username:** eworthe 
       
       **Password:** ballstate0
    3. Enter values into the Subject, Priority, and Description fields.
    4. Click, "Create Bug Report"

+ **Assign an issue**
    1. With the web application launched, navigate to /login.aspx.
    2. Login with the *manager* role using following credentials:
    
       **Username:** swhite 
       
       **Password:** ballstate0
    3. From the *Bugs* drop down menu near the top-left of the page, choose a bug number to assign. Then, select a developer from the *Developer* drop down menu and choose *Assign*.

+ **Update an issue**
    1. With the web application launched, navigate to /login.aspx.
    2. Login with the *developer* role using the following credentials:
    
       **Username:** jaldrich 
       
       **Password:** ballstate0 

+ **Create a user**
    1. With the web application launched, navigate to /login.aspx.
    2. Login with the *administrator* role using the following credentials:
    
       **Username:** jbricker 
       
       **Password:** ballstate0
    3. Complete the Full name, Username, Password, and Type fields. (Passwords must be at least eight (8) characters and include at least one (1) numeral.)
    4. Choose *Create User*
    5. You may now visit /login.aspx to login as the newly-created user and with their assigned role.

Concepts Demonstrated
---------------------
+ Familiarity with the C# programming language.
+ Understanding and execution of Model-View-Controller (MVC) program architecture.
+ Understanding of database architecture fundamentals.
+ Familiarity with SQL and CRUD operations, including the formation of programmatic queries and the interpretation of result sets.
+ Understanding of object-oriented programming practices, including the creation and use of objects.
+ Implementation of a simple, role-based user system.
+ Management of browser sessions.
+ Fundamentals of password encryption.
+ Delivery of "clean code", including the separation of functionality into classes, minimizing method sizes, choice of reasonable method and variable names, consistency in code formatting, appropriate use of comments, and the like.
+ Development and debugging of software in Visual Studio.
+ Use of version control (Git) during software development.