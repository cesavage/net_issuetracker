﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS420_Midterm_CSavage
{
    public class QueryParameter
    {
        public string index;
        public string stringValue;
        public int? intValue;

        public QueryParameter(string parameterIndex, string parameterValue)
        {
            index = parameterIndex;
            stringValue = parameterValue;
        }

        public QueryParameter(string parameterIndex, int parameterValue)
        {
            index = parameterIndex;
            intValue = parameterValue;
        }
    }
}