﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="create-bug.aspx.cs" Inherits="CS420_Midterm_CSavage.create_bug" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frm_createBug" runat="server">
        <div>
        <asp:Label ID="lbl_subject" runat="server" Text="Subject:"></asp:Label>
        </div>
        <asp:TextBox ID="tbx_subject" runat="server"></asp:TextBox>
        <br />
        <br />
        <asp:Label ID="lbl_priority" runat="server" Text="Priority:"></asp:Label>
        <br />
        <asp:DropDownList ID="ddl_priority" runat="server">
            <asp:ListItem Value="low">Low</asp:ListItem>
            <asp:ListItem Value="medium">Medium</asp:ListItem>
            <asp:ListItem Value="high">High</asp:ListItem>
        </asp:DropDownList>
        <br />
        <br />
        <asp:Label ID="lbl_description" runat="server" Text="Description:"></asp:Label>
        <br />
        <asp:TextBox ID="tbx_description" runat="server" Height="78px" MaxLength="50" TextMode="MultiLine" Width="243px"></asp:TextBox>
        <br />
        <br />
        <asp:Button ID="btn_createBug" runat="server" Text="Create Bug Report" OnClick="btn_createBug_Click" />
    </form>
</body>
</html>
