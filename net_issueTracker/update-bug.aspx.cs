﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS420_Midterm_CSavage
{
    public partial class update_bug : System.Web.UI.Page
    {
        DbConnection dbConnection = new DbConnection(ConfigurationManager.ConnectionStrings["bugTrackerDb"].ConnectionString);
        private int bugId;
        private string changes;



        protected void Page_Load(object sender, EventArgs e)
        {//TODO Protect Page
        

            if(!IsPostBack)
            {
                populateBugList();
                populateBugsDropDown();
            }

        }



        private void populateBugList()
        {
            string query = "SELECT Bugs.BugID, Bugs.Subject, Bugs.Priority, Bugs.Description, Bugs.Status, Bugs.Changes, a.Name AS 'Assigned To', e.Name AS 'Entered By' FROM Bugs LEFT JOIN Users AS a ON Bugs.AssignedTo = a.UserID LEFT JOIN Users AS E On Bugs.EnteredBy = e.UserID WHERE Bugs.AssignedTo = @assignedTo AND Bugs.Status = @status" ;
            QueryParameter assignedTo = new QueryParameter("assignedTo", Session["userId"].ToString());
            QueryParameter status = new QueryParameter("status", "Assigned");
            DataSet bugs = dbConnection.getDataSetFromSql(query, "Bugs", assignedTo, status);

            gv_bugs.DataSource = bugs;
            gv_bugs.DataBind();
        }


        private void updateBug()
        {
            string query = "UPDATE Bugs SET Changes = @changes, Status = 'Completed' WHERE BugID = @bugId";
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["bugTrackerDb"].ConnectionString);
            SqlCommand updateCommand = new SqlCommand(query, connection);

            updateCommand.Parameters.AddWithValue("@changes", changes);
            updateCommand.Parameters.AddWithValue("@bugId", bugId);

            connection.Open();
            updateCommand.ExecuteNonQuery();
            connection.Close();

        }

        private void readUserInput()
        {
            bugId = int.Parse(ddl_bugs.SelectedValue.ToString());
            changes = tbx_changes.Text;

        }


        private void populateBugsDropDown()
        {
            string query = "SELECT BugID FROM Bugs WHERE AssignedTo = @assignedTo AND Status = @status";
            QueryParameter assignedTo = new QueryParameter("assignedTo", Session["userId"].ToString());
            QueryParameter statusParameter = new QueryParameter("status", "Assigned");
            DataSet bugs = dbConnection.getDataSetFromSql(query, "Bugs", assignedTo, statusParameter);

            ddl_bugs.DataSource = bugs.Tables["Bugs"];
            ddl_bugs.DataTextField = "BugID";
            ddl_bugs.DataValueField = "BugID";
            ddl_bugs.DataBind();
        }

        protected void btn_fixed_Click(object sender, EventArgs e)
        {
            readUserInput();
            updateBug();
            populateBugList();
            populateBugsDropDown();
        }
    }
}