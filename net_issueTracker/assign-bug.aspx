﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="assign-bug.aspx.cs" Inherits="CS420_Midterm_CSavage.assign_bug" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frm_assignBug" runat="server">
        <div>
            <asp:Label ID="lbl_bugs" Text="Bugs" runat="server" />
            <asp:DropDownList ID="ddl_bugs" runat="server" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="lbl_developers" runat="server" Text="Developers"></asp:Label>
            <asp:DropDownList ID="ddl_developers" runat="server" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             
            <asp:Button ID="btn_assign" runat="server" Text="Assign" OnClick="btn_assign_Click" />
             
            <br />
            <br />

&nbsp;<asp:GridView ID="gv_bugs" runat="server">
            </asp:GridView>
            
        </div>
    </form>
</body>
</html>
