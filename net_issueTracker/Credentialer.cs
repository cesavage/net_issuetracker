﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace CS420_Midterm_CSavage
{
    public class Credentialer
    {
        private string bugTrackerDb = ConfigurationManager.ConnectionStrings["bugTrackerDb"].ConnectionString;
        //private string bugTrackerDb = "Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename = " + Directory.GetCurrentDirectory() + "\\TestData\\QA_testData.mdf; Integrated Security = True; Connect Timeout = 5";
        private string username;
        private string password;

    
    


        //TODO: Login() method could benefit from some additional refactoring.
        public User login(string passedUsername, string passedPassword)
        {
            username = passedUsername;
            password = passedPassword;
            //int userId = -1;
            User loggedInUser = new User();

            DataSet user = getUserWithUsername(passedUsername);

            if(user.Tables["User"].Rows.Count.Equals(1))
            {
                string storedPassword = user.Tables["User"].Rows[0]["Password"].ToString();
                string hashedPassword = hashPassword(passedPassword);

                if (storedPassword.Equals(hashedPassword))
                { 
                    loggedInUser.userId = int.Parse(user.Tables["User"].Rows[0]["UserID"].ToString());
                    loggedInUser.fullName = user.Tables["User"].Rows[0]["Name"].ToString();
                    loggedInUser.type = user.Tables["User"].Rows[0]["Type"].ToString();


                    //try
                    //{
                    //    HttpContext.Current.Session.
                    //    HttpContext.Current.Session.Add("userId", userId);
                    //    HttpContext.Current.Session.Add("userFullName", user.Tables["User"].Rows[0]["Name"].ToString());
                    //    HttpContext.Current.Session.Add("userType", user.Tables["User"].Rows[0]["Type"].ToString());

                    //    //HttpContext.Current.Session["userFullName"] = user.Tables["User"].Rows[0]["Name"].ToString();
                    //    //HttpContext.Current.Session["userType"] = user.Tables["User"].Rows[0]["Type"].ToString();
                    //}
                    //catch (Exception HttpContextDoesNotExist)
                    //{
                    //    //HttpContext doesn't exist during unit testing.
                    //}

                }
            }

            

            return loggedInUser;
        }


        private DataSet getUserWithUsername(string username)
        {
            DbConnection dbConnection = new DbConnection(bugTrackerDb);

            string findUserQuery = "SELECT * FROM Users WHERE Login = @username";
            QueryParameter usernameParameter = new QueryParameter("username", username);
            DataSet user = dbConnection.getDataSetFromSql(findUserQuery, "User", usernameParameter);

            return user;
        }




        public Boolean checkIfUsernameIsAvailable(string newUsername)
        {
            DataSet usersWithUsername = getUserWithUsername(newUsername);

            if (usersWithUsername.Tables["User"].Rows.Count > 0)
            {
                return false;
            }

            else
            {
                return true;
            }
        }




        public Boolean checkMinPasswordLengthMet(string password)
        {
            if (password.Length >= 8)
            {
                return true;
            }

            else
            {
                return false;
            }
        }




        public Boolean checkPasswordComplexityMet(string password)
        {
            if (!password.All(Char.IsLetter) && !password.All(Char.IsDigit))
            {
                return true;
            }

            else
            {
                return false;
            }
        }




        public string hashPassword(string password)
        {
            SHA1 cryptoProvider = new SHA1CryptoServiceProvider();
            cryptoProvider.ComputeHash(ASCIIEncoding.ASCII.GetBytes(password));
            byte[] result = cryptoProvider.Hash;

            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                stringBuilder.Append(result[i].ToString("x2"));
            }

            return stringBuilder.ToString();
        }

    }
}