﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update-bug.aspx.cs" Inherits="CS420_Midterm_CSavage.update_bug" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frm_updateBug" runat="server">
        <div>
           <asp:GridView ID="gv_bugs" runat="server" /><br />
            <br />
            <asp:Label ID="Bug" Text="Bug:" runat="server" /><br />
            <br />
            <asp:DropDownList ID="ddl_bugs" runat="server" /><br />
            <br />
            <asp:Label ID="lbl_changes" Text="Changes:" runat="server"/><br />
            <asp:TextBox ID="tbx_changes" runat="server" /><br />
            <br />
            <asp:Button ID="btn_fixed" Text="Fixed" runat="server" OnClick="btn_fixed_Click" />
        </div>
    </form>
</body>
</html>
