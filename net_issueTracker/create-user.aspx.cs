﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS420_Midterm_CSavage
{
    public partial class create_user : System.Web.UI.Page
    {
        User newUser = new User();
        private string fullName;
        private string username;
        private string password;
        private string type;


        protected void Page_Load(object sender, EventArgs e)
        {
            //TODO check if user is valid.
        }

        private void readUserInput()
        {
            fullName = tbx_fullName.Text;
            username = tbx_username.Text;
            password = tbx_password.Text;
            type = ddl_userType.SelectedValue;
        }

        private void createUser()
        {
            newUser.fullName = fullName;
            newUser.username = username;
            newUser.password = password;
            newUser.type = type;
        }

        protected void btn_createUser_Click(object sender, EventArgs e)
        {
            readUserInput();
            createUser();
            Credentialer credentialer = new Credentialer();

            Boolean usernameIsAvailable = credentialer.checkIfUsernameIsAvailable(newUser.username);

            if (!usernameIsAvailable)
            {
                Response.Write("The username " + newUser.username + " is already in use.");
                return;
            }

            Boolean passwordLengthOK = credentialer.checkMinPasswordLengthMet(newUser.password);
            if (!passwordLengthOK)
            {
                Response.Write("Passwords must be at least 8 characters.");
                return;
            }

            Boolean passwordComplexityOK = credentialer.checkPasswordComplexityMet(newUser.password);
            if(!passwordComplexityOK)
            {
                Response.Write("Passwords must contain at least one letter and one number.");
                return;
            }

            DbConnection dbConnection = new DbConnection(ConfigurationManager.ConnectionStrings["bugTrackerDb"].ConnectionString);
            dbConnection.addUserToDatabase(newUser);
        }
    }
}