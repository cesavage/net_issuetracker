﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CS420_Midterm_CSavage
{
    public class Bug
    {
        public int BugId;
        public int enteredBy;
        public string subject;
        public string priority;
        public string description;
        public int assignedTo;
        public string status;
        public string changes;
    }
}