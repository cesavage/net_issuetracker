﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CS420_Midterm_CSavage
{
    public class DbConnection
    {
        SqlConnection dbConnection;




        public DbConnection(string connectionString)
        {
            this.dbConnection = new SqlConnection(connectionString);
        }




        public DataSet getDataSetFromSql(string query, string tableName)
        {
            SqlCommand dbCommand = new SqlCommand(query, dbConnection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(dbCommand);

            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet, tableName);

            return dataSet;
        }



        public DataSet getDataSetFromSql(string query, string tableName, params QueryParameter[] sqlParameters)
        {
            SqlCommand dbCommand = new SqlCommand(query, dbConnection);

            for (int charIndex = 0; charIndex < query.Length; charIndex++)
            {
                char currentCharacter = query[charIndex];

                if (characterIsParameterDelimiter(currentCharacter))
                {
                    string parameter = getParameterFromQuery(query, charIndex);
                    dbCommand = addParameterToCommand(parameter, sqlParameters, dbCommand);
                }
            }

            SqlDataAdapter dataAdapter = new SqlDataAdapter(dbCommand);

            DataSet dataSet = new DataSet();
            dataAdapter.Fill(dataSet, tableName);

            return dataSet;
        }




        private Boolean characterIsParameterDelimiter(Char character)
        {
            if (character.Equals('@'))
            {
                return true;
            }

            else
            {
                return false;
            }
        }




        private Boolean characterIsLastInString(int charIndex, string query)
        {
            if ((charIndex + 1).Equals(query.Length))
            {
                return true;
            }

            else
            {
                return false;
            }
        }




        private String getParameterFromQuery(string query, int paramStartIndex)
        {
            char currentCharacter;
            string parameter = "unsetParameter";
            int paramEndIndex = -1;

            for (int charIndex = paramStartIndex; charIndex < query.Length; charIndex++)
            {
                currentCharacter = query[charIndex];

                if (currentCharacter.Equals(' ') || currentCharacter.Equals(')'))
                {
                    paramEndIndex = charIndex - 1;
                    parameter = query.Substring((int)paramStartIndex + 1, (int)paramEndIndex - (int)paramStartIndex);
                    return parameter;
                }

                else if ((charIndex + 1).Equals(query.Length))
                {
                    paramEndIndex = charIndex;
                    parameter = query.Substring((int)paramStartIndex + 1, (int)paramEndIndex - (int)paramStartIndex);
                    return parameter;
                }

            }

            return parameter;
        }




        private SqlCommand addParameterToCommand(string parameter, QueryParameter[] sqlParameters, SqlCommand dbCommand)
        {
            foreach (QueryParameter sqlParameter in sqlParameters)
            {
                if (sqlParameter.index.Equals(parameter))
                {
                    if (sqlParameter.stringValue != null)
                    {
                        dbCommand.Parameters.AddWithValue("@" + parameter, sqlParameter.stringValue);
                    }

                    else
                    {
                        dbCommand.Parameters.AddWithValue("@" + parameter, sqlParameter.intValue);
                    }
                    
                }
            }
            return dbCommand;
        }




        public int addUserToDatabase(User newUser)
        {
            string queryMaxUserId = "SELECT MAX(UserID) FROM Users";
            DataSet maxId = new DataSet();

            maxId = getDataSetFromSql(queryMaxUserId, "userIds");
            int userId = int.Parse(maxId.Tables["userIds"].Rows[0][0].ToString());
            userId += 1;

            Credentialer credentialer = new Credentialer();
            string hashedPassword = credentialer.hashPassword(newUser.password);

            string queryInsertUser = "INSERT INTO Users (UserID, Name, Login, Password, Type) VALUES (@UserID, @Name, @Login, @Password, @Type)";
            SqlCommand insertCommand = new SqlCommand(queryInsertUser, dbConnection);

            insertCommand.Parameters.AddWithValue("UserID", userId);
            insertCommand.Parameters.AddWithValue("Name", newUser.fullName);
            insertCommand.Parameters.AddWithValue("Login", newUser.username);
            insertCommand.Parameters.AddWithValue("Password", hashedPassword);
            insertCommand.Parameters.AddWithValue("Type", newUser.type);

            dbConnection.Open();
            insertCommand.ExecuteNonQuery();
            dbConnection.Close();

            return userId;
        }





        public void addBugToDatabase(Bug newBug)
        {
            //string queryMaxUserId = "SELECT MAX(UserID) FROM Users";
            //DataSet maxId = new DataSet();

            //maxId = getDataSetFromSql(queryMaxUserId, "userIds");
            //int userId = int.Parse(maxId.Tables["userIds"].Rows[0][0].ToString());
            //userId += 1;

            //Credentialer credentialer = new Credentialer();
            //string hashedPassword = credentialer.hashPassword(newUser.password);

            string queryInsertBug = "INSERT INTO Bugs (EnteredBy, Subject, Priority, Description, Status) VALUES (@EnteredBy, @Subject, @Priority, @Description, @Status)";
            SqlCommand insertCommand = new SqlCommand(queryInsertBug, dbConnection);

            insertCommand.Parameters.AddWithValue("EnteredBy", newBug.enteredBy);
            insertCommand.Parameters.AddWithValue("Subject", newBug.subject);
            insertCommand.Parameters.AddWithValue("Priority", newBug.priority);
            insertCommand.Parameters.AddWithValue("Description", newBug.description);
            insertCommand.Parameters.AddWithValue("Status", newBug.status);



            dbConnection.Open();
            insertCommand.ExecuteNonQuery();
            dbConnection.Close();
        }


        public void assignBug(int bugId, int developerId)
        {
            string queryAssignBug = "UPDATE Bugs SET AssignedTo = @a, Status = 'Assigned' WHERE BugID = @b";
            //string queryAssignBug = "UPDATE Bugs SET AssignedTo = '9' WHERE BugID = '1'";
            SqlCommand updateCommand = new SqlCommand(queryAssignBug, dbConnection);

            updateCommand.Parameters.AddWithValue("a", developerId);
            updateCommand.Parameters.AddWithValue("b", bugId);

            dbConnection.Open();
            updateCommand.ExecuteNonQuery();
            dbConnection.Close();
        }

    }






    }
