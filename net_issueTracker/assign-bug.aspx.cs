﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS420_Midterm_CSavage
{
    public partial class assign_bug : System.Web.UI.Page
    {
        DbConnection dbConnection = new DbConnection(ConfigurationManager.ConnectionStrings["bugTrackerDb"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                populateBugList();
                populateBugsDropDown();
                populateDevelopersDropDown();
            }
        }

        private void populateBugList()
        {
            string query = "SELECT Bugs.BugID, Bugs.Subject, Bugs.Priority, Bugs.Description, Bugs.Status, Bugs.Changes, a.Name AS 'Assigned To', e.Name AS 'Entered By' FROM Bugs LEFT JOIN Users AS a ON Bugs.AssignedTo = a.UserID LEFT JOIN Users AS E On Bugs.EnteredBy = e.UserID";
            DataSet bugs = dbConnection.getDataSetFromSql(query, "Bugs");

            gv_bugs.DataSource = bugs;
            gv_bugs.DataBind();
        }

        private void populateBugsDropDown()
        {
            string query = "SELECT BugID FROM Bugs";
            DataSet bugs = dbConnection.getDataSetFromSql(query, "Bugs");

            ddl_bugs.DataSource = bugs.Tables["Bugs"];
            ddl_bugs.DataTextField = "BugID";
            ddl_bugs.DataValueField = "BugID";
            ddl_bugs.DataBind();
        }

        private void populateDevelopersDropDown()
        {
            string query = "SELECT Name, UserID FROM Users WHERE Type = 'Developer'";
            DataSet developers = dbConnection.getDataSetFromSql(query, "Developers");

            ddl_developers.DataSource = developers.Tables["Developers"];
            ddl_developers.DataTextField = "Name";
            ddl_developers.DataValueField = "UserID";
            ddl_developers.DataBind();
        }


        protected void btn_assign_Click(object sender, EventArgs e)
        {
            dbConnection.assignBug(int.Parse(ddl_bugs.SelectedValue), int.Parse(ddl_developers.SelectedValue));
            populateBugList();
        }
    }
}