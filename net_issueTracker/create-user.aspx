﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="create-user.aspx.cs" Inherits="CS420_Midterm_CSavage.create_user" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frm_addUser" runat="server">
        <div>
            <asp:Label ID="lbl_fullName" Text="Full name:" runat="server"/>
            <br />
            <asp:TextBox ID="tbx_fullName" runat="server" />
            <br />
            <br />
            <asp:Label ID="lblUsername" Text="Username:" runat="server" />
            <br />
            <asp:TextBox ID="tbx_username" runat="server" />
            <br />
            <br />
            <asp:Label ID="lbl_password" Text="Password:" runat="server" />
            <br />
            <asp:TextBox ID="tbx_password" runat="server" TextMode="Password" />
            <br />
            <br />
            <asp:Label ID="lbl_type" Text="Type:" runat="server" />
            <br />
            <asp:DropDownList ID="ddl_userType" runat="server" >
                <asp:ListItem Value="tester">Tester</asp:ListItem>
                <asp:ListItem Value="developer">Developer</asp:ListItem>
                <asp:ListItem Value="manager">Manager</asp:ListItem>
                <asp:ListItem Value="administrator">Administrator</asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
            <asp:Button ID="btn_createUser" Text="Create User" runat="server" OnClick="btn_createUser_Click" />
        </div>
    </form>
</body>
</html>
