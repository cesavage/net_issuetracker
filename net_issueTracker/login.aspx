﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="CS420_Midterm_CSavage.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="frm_login" runat="server">
        <div>
            <asp:Label ID="lbl_userName" runat="server" Text="Username: " />
            <asp:TextBox ID="tbx_userName" runat="server" />

            <br />

            <asp:Label ID="lbl_password" runat="server" Text="Password: " />
            <asp:TextBox ID="tbx_password" runat="server" TextMode="Password" />

            <br />

            <asp:Button ID="btn_login" runat="server" Text="Login" OnClick="btn_login_Click" />
        </div>
    </form>
</body>
</html>
