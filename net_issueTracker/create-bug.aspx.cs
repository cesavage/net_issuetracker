﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS420_Midterm_CSavage
{
    public partial class create_bug : System.Web.UI.Page
    {
        string subject = "";
        string priority = "";
        string description = "";
        int? assignedTo = null;
        string status = "Open";
        string changes = "";

        Bug newBug = new Bug();

        protected void Page_Load(object sender, EventArgs e)
        {
            //TODO Protect page.
        }

        private void readUserInput()
        {
            subject = tbx_subject.Text;
            priority = ddl_priority.SelectedValue;
            description = tbx_description.Text;
        }

        private void createBug()
        {
            newBug.subject = subject;
            newBug.priority = priority;
            newBug.description = description;
            newBug.enteredBy = int.Parse(Session["userId"].ToString());
            newBug.status = status;
            newBug.description = description;
            newBug.changes = changes;
        }

        protected void btn_createBug_Click(object sender, EventArgs e)
        {
            readUserInput();
            createBug();

            DbConnection dbConnection = new DbConnection(ConfigurationManager.ConnectionStrings["bugTrackerDb"].ConnectionString);
            dbConnection.addBugToDatabase(newBug);
        }
    }
}