﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CS420_Midterm_CSavage
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        private string username;
        private string password;

        protected void Page_Load(object sender, EventArgs e)
        {

        }




        private void readUserInput()
        {
            username = tbx_userName.Text.ToString();
            password = tbx_password.Text.ToString();
        }




        protected void btn_login_Click(object sender, EventArgs e)
        {
            readUserInput();

            Credentialer currentUser = new Credentialer();
            User loggedInUser = currentUser.login(username, password);

            if (loggedInUser.type != null)
            {
                Session["userId"] = loggedInUser.userId;
                Session["fullName"] = loggedInUser.fullName;
                Session["type"] = loggedInUser.type;

                if(Session["type"].Equals("Administrator"))
                {
                    Response.Redirect("create-user.aspx");
                }

                else if(Session["type"].Equals("Tester"))
                {
                    Response.Redirect("create-bug.aspx");
                }

                else if(Session["type"].Equals("Developer"))
                {
                    Response.Redirect("update-bug.aspx");
                }

                else if(Session["type"].Equals("Manager"))
                {
                    Response.Redirect("assign-bug.aspx");
                }
            }

            else
            {
                Response.Write("Username/password invalid.");
            }




            
            //ensure username exists
            //if so, get associated password hash

            //hash input password
            //compare input password to password hash from db

            //if the same
            //create session with user
            
        }
    }
}