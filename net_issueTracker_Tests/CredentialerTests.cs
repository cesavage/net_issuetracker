﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CS420_Midterm_CSavage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS420_Midterm_CSavage.Tests
{
    [TestClass()]
    public class CredentialerTests
    {
       
        [TestMethod()]
        public void hashPasswordTest()
        {
            string plaintextPassword = "password";
            Credentialer credentialer = new Credentialer();
            string hashedPassword = credentialer.hashPassword(plaintextPassword);

            Assert.AreEqual("5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8", hashedPassword);
        }

        [TestMethod()]
        public void checkIfUsernameIsAvailable_false()
        {
            Credentialer credentialer = new Credentialer();
            Boolean usernameIsUsed = credentialer.checkIfUsernameIsAvailable("jaldrich");

            Assert.AreEqual(false, usernameIsUsed);
        }

        [TestMethod()]
        public void checkIfUsernameIsAvailable_true()
        {
            Credentialer credentialer = new Credentialer();
            Boolean usernameIsUsed = credentialer.checkIfUsernameIsAvailable("nonsenseUserName");

            Assert.AreEqual(true, usernameIsUsed);
        }

        [TestMethod()]
        public void checkMinPasswordLengthMetTest_true()
        {
            Credentialer credentialer = new Credentialer();
            Boolean lengthOK = credentialer.checkMinPasswordLengthMet("12345678");

            Assert.IsTrue(lengthOK);
        }

        [TestMethod()]
        public void checkMinPasswordLengthMetTest_false()
        {
            Credentialer credentialer = new Credentialer();
            Boolean lengthOK = credentialer.checkMinPasswordLengthMet("1234567");

            Assert.IsFalse(lengthOK);
        }

        [TestMethod()]
        public void checkPasswordComplexityMetTest_allDigits()
        {
            Credentialer credentialer = new Credentialer();
            {
                Boolean complexityOK = credentialer.checkPasswordComplexityMet("12345678");

                Assert.IsFalse(complexityOK);
            }
        }

        [TestMethod()]
        public void checkPasswordComplexityMetTest_allLetters()
        {
            Credentialer credentialer = new Credentialer();
            {
                Boolean complexityOK = credentialer.checkPasswordComplexityMet("abcdefgh");

                Assert.IsFalse(complexityOK);
            }
        }

        [TestMethod()]
        public void checkPasswordComplexityMetTest_lettersAndNumbers()
        {
            Credentialer credentialer = new Credentialer();
            {
                Boolean complexityOK = credentialer.checkPasswordComplexityMet("abcd1234");

                Assert.IsTrue(complexityOK);
            }
        }
    }
}