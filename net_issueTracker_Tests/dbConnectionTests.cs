﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CS420_Midterm_CSavage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Reflection;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Data;

namespace CS420_Midterm_CSavage.Tests
{
    [TestClass()]
    public class dbConnectionTests
    {
        static string testDbConnectionString = "Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename = " + Directory.GetCurrentDirectory() + "\\TestData\\QA_testData.mdf; Integrated Security = True; Connect Timeout = 5";




        [TestMethod()]
        public void testDbConnection()
        {
            string testDbConnectionString = "Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename = " + Directory.GetCurrentDirectory() + "\\TestData\\QA_testData.mdf; Integrated Security = True; Connect Timeout = 30";
            SqlConnection testDbConnection = new SqlConnection(testDbConnectionString);

            try
            {
                testDbConnection.Open();
                testDbConnection.Close();
            }

            catch (Exception)
            {
                Assert.Fail("Could not connect to test database.");
            }
        }




        [TestMethod()]
        public void test_resetTestDb()
        {
            insertTestUserRecord();
            insertTestBugRecord();

            int userCountAfterTestInsert = getUserCountFromTestDb();
            int bugCountAfterTestInsert = getBugCountFromTestDb();


            Assert.AreEqual(6, userCountAfterTestInsert, 0, "Increased number of users by (1) by adding an additional user record to the test database.");
            Assert.AreEqual(1, bugCountAfterTestInsert, 0, "Increased number of bugs by (1) by adding an additional bug record to the test datbase.");

            resetTestDb();

            int userCountAfterReset = getUserCountFromTestDb();
            int bugCountAfterReset = getBugCountFromTestDb();

            Assert.AreEqual(5, userCountAfterReset, 0, "Confirmed the test user table has been reset to the expected default number of records.");
            Assert.AreEqual(3, bugCountAfterReset, 0, "Confirmed the test bugs table has been reset to the expected default number of records.");
        }




        [TestMethod()]
        public void getDataSetFromSqlTest()
        {
            string sql = "SELECT * FROM Users";
            string tableName = "Users";

            DataSet returnedDataSet = new DataSet();
            DbConnection dbConnection = new DbConnection(testDbConnectionString);

            returnedDataSet = dbConnection.getDataSetFromSql(sql, tableName);

            int returnedRowCount = returnedDataSet.Tables["Users"].Rows.Count;
            Assert.AreEqual(5, returnedRowCount);

            resetTestDb();
        }




        [TestMethod()]
        public void getDataSetFromParamaterizedSqlTest()
        {
            string sql = "SELECT * FROM Users WHERE Type = @type";
            QueryParameter majorParameter = new QueryParameter("type", "Tester");

            string tableName = "Users";

            DataSet returnedDataSet = new DataSet();
            DbConnection dbConnection = new DbConnection(testDbConnectionString);

            returnedDataSet = dbConnection.getDataSetFromSql(sql, tableName, majorParameter);

            int returnedRowCount = returnedDataSet.Tables["Users"].Rows.Count;
            Assert.AreEqual(2, returnedRowCount);

            resetTestDb();
        }




        [TestMethod()]
        public void createUserTest()
        {
            User newUser = new User();
            newUser.fullName = "Test User";
            newUser.username = "tuser";
            newUser.password = "password";
            newUser.type = "Tester";

            DbConnection dbConnection = new DbConnection(testDbConnectionString);
            dbConnection.addUserToDatabase(newUser);

            string query = "SELECT Name FROM Users WHERE Login = 'tuser'";
            DataSet users = new DataSet();
            
            users = dbConnection.getDataSetFromSql(query, "Users");

            int usersFound = users.Tables["Users"].Rows.Count;

            Assert.AreEqual(1, usersFound);

            resetTestDb();
        }



        [TestMethod()]
        public void testAddBug()
        {
            Bug newBug = new Bug();

            newBug.enteredBy = 1;
            newBug.subject = "Test subject";
            newBug.priority = "Low";
            newBug.description = "Test description";
            newBug.assignedTo = 0;
            newBug.status = "Open";
            newBug.changes = "Test changes";

            DbConnection dbConnection = new DbConnection(testDbConnectionString);
            dbConnection.addBugToDatabase(newBug);

            string query = "SELECT BugID FROM Bugs";
            DataSet users = new DataSet();

            users = dbConnection.getDataSetFromSql(query, "Bugs");

            int usersFound = users.Tables["Bugs"].Rows.Count;

            Assert.AreEqual(4, usersFound);
        }















        public void insertTestUserRecord()
        {
            SqlConnection testDbConnection = new SqlConnection(testDbConnectionString);

            string insertUserQuery = "INSERT INTO Users(UserID, Name, Login, Password, Type) VALUES(99, 'Test User', 'tuser', 'password', 'type')";
            SqlCommand insertUserCommand = new SqlCommand(insertUserQuery, testDbConnection);

            testDbConnection.Open();

            insertUserCommand.ExecuteNonQuery();

            testDbConnection.Close();
        }


        public void insertTestBugRecord()
        {
            SqlConnection testDbConnection = new SqlConnection(testDbConnectionString);

            string insertBugQuery = "INSERT INTO Bugs (EnteredBy, Subject, Priority, Description, AssignedTo, Status, Changes) VALUES ('1', 'Subject', 'Priority', 'Description', '1', 'Status', 'Changes')";
            SqlCommand insertBugCommand = new SqlCommand(insertBugQuery, testDbConnection);

            testDbConnection.Open();

            insertBugCommand.ExecuteNonQuery();

            testDbConnection.Close();
        }


        public void resetTestDb()
        {
            emptyTable("Users");
            emptyTable("Bugs");

            Dictionary<int, Dictionary<string, string>> users = generateTestUsers();
            Dictionary<int, Dictionary<string, string>> bugs = generateTestBugs();

            populateUsersTable(users);
            populateBugsTable(bugs);
        }


        public void emptyTable(string tableName)
        {
            SqlConnection testDbConnection = new SqlConnection(testDbConnectionString);
            string deleteQuery = "DELETE FROM " + tableName;
            SqlCommand deleteCommand = new SqlCommand(deleteQuery, testDbConnection);

            testDbConnection.Open();
            deleteCommand.ExecuteNonQuery();
            testDbConnection.Close();
        }


        public void populateUsersTable(Dictionary<int, Dictionary<string, string>> users)
        {
            SqlConnection testDbConnection = new SqlConnection(testDbConnectionString);

            testDbConnection.Open();

            foreach (KeyValuePair<int, Dictionary<string, string>> user in users)
            {
                int userId = int.Parse(user.Value["userId"]);
                string name = user.Value["name"];
                string login = user.Value["login"];
                string password = user.Value["password"];
                string type = user.Value["type"];

                string query = "INSERT INTO Users (UserID, Name, Login, Password, Type) VALUES ('" + userId + "', '" + name + "', '" + login + "', '" + password + "', '" + type + "')";
                //string query = "INSERT INTO Users VALUES ('9', '9', '9', '9', '9')";
                SqlCommand command = new SqlCommand(query, testDbConnection);

                command.ExecuteNonQuery();
            }

            testDbConnection.Close();
        }

        
        public void populateBugsTable(Dictionary<int, Dictionary<string, string>> bugs)
        {
            SqlConnection testDbConnection = new SqlConnection(testDbConnectionString);

            testDbConnection.Open();

            foreach (KeyValuePair<int, Dictionary<string, string>> bug in bugs)
            {
                int bugId = int.Parse(bug.Value["bugId"]);
                string enteredBy = bug.Value["enteredBy"];
                string subject = bug.Value["subject"];
                string priority = bug.Value["priority"];
                string description = bug.Value["description"];
                string assignedTo = bug.Value["assignedTo"];
                string status = bug.Value["status"];
                string changes = bug.Value["changes"];


                string query = "INSERT INTO Bugs (EnteredBy, Subject, Priority, Description, AssignedTo, Status, Changes) VALUES ('" + enteredBy + "', '" + subject + "', '" + priority + "', '" + description + "', '" + assignedTo + "', '" + status + "', '" + changes + "')";
                SqlCommand command = new SqlCommand(query, testDbConnection);

                command.ExecuteNonQuery();

            }

            testDbConnection.Close();
        }


        public Dictionary<int, Dictionary<string, string>> generateTestUsers()
        {
            Dictionary<string, string> userId0 = new Dictionary<string, string>
            {
                {"userId", "0" },
                {"name", "John Aldrich" },
                {"login", "jaldrich" },
                {"password", "311b2d4934d932c8a812e4d89c43f278d8f96214" },
                {"type", "Developer" }
            };

            Dictionary<string, string> userId1 = new Dictionary<string, string>
            {
                {"userId", "1" },
                {"name", "Kevin Johnson" },
                {"login", "kjohnso" },
                {"password", "311b2d4934d932c8a812e4d89c43f278d8f96214" },
                {"type", "Tester" }
            };

            Dictionary<string, string> userId2 = new Dictionary<string, string>
            {
                {"userId", "2" },
                {"name", "Sarah White" },
                {"login", "swhite" },
                {"password", "311b2d4934d932c8a812e4d89c43f278d8f96214" },
                {"type", "Manager" }
            };

            Dictionary<string, string> userId3 = new Dictionary<string, string>
            {
                {"userId", "3" },
                {"name", "Emily Worthen" },
                {"login", "eworthe" },
                {"password", "311b2d4934d932c8a812e4d89c43f278d8f96214" },
                {"type", "Tester" }
            };

            Dictionary<string, string> userId4 = new Dictionary<string, string>
            {
                {"userId", "4" },
                {"name", "Josh Bricker" },
                {"login", "jbricker" },
                {"password", "311b2d4934d932c8a812e4d89c43f278d8f96214" },
                {"type", "Administrator" }
            };


            Dictionary<int, Dictionary<string, string>> users = new Dictionary<int, Dictionary<string, string>>
            {
                {0, userId0 },
                {1, userId1 },
                {2, userId2 },
                {3, userId3 },
                {4, userId4 },
            };

            return users;
        }



        public Dictionary<int, Dictionary<string, string>> generateTestBugs()
        {
            Dictionary<string, string> bug0 = new Dictionary<string, string>
            {
                {"bugId", "0" },
                {"enteredBy", "1"},
                {"subject", "Login form" },
                {"priority", "Medium" },
                {"description", "The login form does not exist yet." },
                {"assignedTo", "0" },
                {"status", "Assigned" },
                {"changes", "The log in form will need to be added." }

            };


            Dictionary<string, string> bug1 = new Dictionary<string, string>
            {
                {"bugId", "1" },
                {"enteredBy", "3"},
                {"subject", "Pictures are not loading" },
                {"priority", "Low" },
                {"description", "No pictures are loading properly." },
                {"assignedTo", "" },
                {"status", "Open" },
                {"changes", "Check all img tags." }
            };


            Dictionary<string, string> bug2 = new Dictionary<string, string>
            {
                {"bugId", "2" },
                {"enteredBy", "3"},
                {"subject", "Apache config needs completed." },
                {"priority", "Low" },
                {"description", "Configure apache to serve sites from vhosts." },
                {"assignedTo", "0" },
                {"status", "Completed" },
                {"changes", "Changed apache to allow vhosts on port 80." }
            };


            Dictionary<int, Dictionary<string, string>> bugs = new Dictionary<int, Dictionary<string, string>>
            {
                {0, bug0 },
                {1, bug1 },
                {2, bug2 },
            };

            return bugs;
        }


        public int getUserCountFromTestDb()
        {
            SqlConnection testDbConnection = new SqlConnection(testDbConnectionString);

            string selectUsersQuery = "SELECT * FROM Users";
            SqlCommand selectUsersCommand = new SqlCommand(selectUsersQuery, testDbConnection);

            DataSet dbData = new DataSet();
            SqlDataAdapter usersDataAdapter = new SqlDataAdapter(selectUsersCommand);

            usersDataAdapter.Fill(dbData, "Users");

            return dbData.Tables["Users"].Rows.Count;
        }


        public int getBugCountFromTestDb()
        {
            SqlConnection testDbConnection = new SqlConnection(testDbConnectionString);

            string selectBugsQuery = "SELECT * FROM Bugs";
            SqlCommand selectBugsCommand = new SqlCommand(selectBugsQuery, testDbConnection);

            DataSet dbData = new DataSet();
            SqlDataAdapter bugsDataAdapter = new SqlDataAdapter(selectBugsCommand);

            bugsDataAdapter.Fill(dbData, "Bugs");

            return dbData.Tables["Bugs"].Rows.Count;
        }











    }
}